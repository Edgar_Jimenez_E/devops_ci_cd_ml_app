import pickle
import matplotlib.pyplot as plt

from sklearn.datasets import load_wine
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import plot_confusion_matrix

# load data
data = load_wine(as_frame=True)
# Train / Test split
X_train, X_test, y_train, y_test = train_test_split(data.data, data.target, random_state=42)

# n_estimators=10,random_state=42
# Fit the classifier
rf_clf = RandomForestClassifier(n_estimators=10, random_state=42).fit(X_train, y_train)

with open('wine.pkl', 'wb') as files:
    pickle.dump(rf_clf, files)

# Print the mean accuracy achieved by the classifier on the test set
acc = rf_clf.score(X_test, y_test)
print("mean accuracy :", acc)
with open("metrics.txt", 'w') as outfile:
    outfile.write("Accuracy: " + str(acc) + "\n")

# Plot confusion_matrix.png
disp = plot_confusion_matrix(rf_clf, X_test, y_test, normalize='true', cmap=plt.cm.Blues)
plt.savefig('confusion_matrix.png')
